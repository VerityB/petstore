<?php

namespace Drupal\pet_blog\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Client;

class PetBlogController extends ControllerBase {

  /** 
  * The HTTP connector client.
  *
  * @var GuzzleHttp\Client
  */
  protected $httpClient;

  /**
  * Construct the controller.
  *
  * @param \GuzzleHttp\Client $client
  *   The HTTP connector.
  */
  public function __construct(Client $http_client) {
    $this->httpClient = $http_client;
  }

  /** 
  * {@inheritdoc}
  */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('http_client')
    ); 
  }

  /**
  * Sends data to the module template. 
  * 
  * @return array
  */
  public function content() {
    return [
      '#theme' => 'pet_blog_post_list',
      '#posts' => json_decode($this->posts()), 
      '#title' => 'Friends Pet Blog', 
    ];
  }

  /**
  * The get request. 
  *
  * @return json 
  */
  public function posts(){ 
    $posts = $this->httpClient->request('GET', 'http://jsonplaceholder.typicode.com/posts?_limit=10', [
    ])->getBody()->getContents();

    return $posts;
  }

}
