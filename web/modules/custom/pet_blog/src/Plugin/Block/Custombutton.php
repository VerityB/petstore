<?php

namespace Drupal\pet_blog\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\pet_blog\Controller\PetBlogController;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * 
 * Provides a block with a simple text.
 *
 * @Block(
 *   id = "Button Block",
 *   admin_label = @Translation("Custom button block."),
 *   category = @Translation("PetStore Blocks"),
 * )
 */
class Custombutton extends BlockBase implements BlockPluginInterface {
   /**
   * {@inheritdoc}
   */
  public function build() {
    return [
      '#theme' => 'custombutton',
      '#text' => $this->configuration['custom_button_text'],
      '#url' => $this->configuration['custom_button_url'],
    ];
  }

   /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);

    $config = $this->getConfiguration();

    $form['custom_button_text'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button Text'),
      '#description' => $this->t('Fill in the text for the button title.'),
      '#default_value' => isset($config['custom_button_text']) ? $config['custom_button_text'] : '',
    ];

    $form['custom_button_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Button URL'),
      '#description' => $this->t('Fill in the URL for the button link.'),
      '#default_value' => isset($config['custom_button_url']) ? $config['custom_button_url'] : '',
    ];

    return $form;
  }

   /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
    $values = $form_state->getValues();
    $this->configuration['custom_button_text'] = $values['custom_button_text'];
    $this->configuration['custom_button_url'] = $values['custom_button_url'];
  }
}