<?php

namespace Drupal\pet_blog\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\pet_blog\Controller\PetBlogController;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use GuzzleHttp\Client;

  /**
   * Provides a block with articles from an external source.
   *
   * @Block(
   *   id = "PetBlogBlock",
   *   admin_label = @Translation("Shows a single entry from a blog."),
   *   category = @Translation("PetStore Blocks"),
   * )
   */
class PetBlogBlock extends BlockBase implements ContainerFactoryPluginInterface {
   
  /** 
   * The HTTP connector client.
   *
   * @var GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * Create a plugin with the given input.
   * 
   * @param array $configuration
   *   The configuration of the plugin.
   * @param string $plugin_id
   *   The plugin id.
   * @param mixed $plugin_definition   
   *   The plugin implementation definition.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   An HTTP client.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Client $http_client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->httpClient = $http_client;
  }

  /**
   * Creates an instance of the plugin.
   * 
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container to pull out services used in the plugin.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   *
   * @return static
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('http_client')
    );
  }

  /**
  * {@inheritdoc}
  */
  public function build() {
    return [
      '#theme' => 'pet_blog_post_list',
      '#posts' => json_decode($this->posts()),
      '#title' => 'Friends Pet Blog', 
    ];
  }

  /**
  * The get request. 
  *
  * @return json 
  */
  public function posts(){ 
    $posts = $this->httpClient->request('GET', 'http://jsonplaceholder.typicode.com/posts?_limit=1', [
    ])->getBody()->getContents();

    return $posts;
  }
}
