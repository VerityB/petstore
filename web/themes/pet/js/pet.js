(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.pet = {
    attach: function (context, settings) {
      $('.menu-wrapper').on('click', function() {
        $('.hamburger-menu').toggleClass('animate');
        $('.menu-item a').toggleClass('visible');
        $('.menu--main').toggleClass('visible');
      })
    }
  };

})(jQuery, Drupal, drupalSettings);
